import PyQt5
from PyQt5.QtWidgets import QWidget, QDesktopWidget, QLabel, QApplication
from PyQt5.QtCore import QRect, Qt, QMimeData
from PyQt5.QtGui import QIcon, QPixmap, QDrag


class ScreenWidget(QLabel):
    def __init__(self, desktop : QDesktopWidget, frame : QRect, number : int, parent = None):
        super().__init__(parent)
        self._number = number
        self.event_pos = None
        self.geometry = desktop.screenGeometry()

        while self.geometry.width() > frame.width()/2:
            self.geometry.setWidth(self.geometry.width()/2)

        while self.geometry.height() > frame.height()/2:
            self.geometry.setHeight(self.geometry.height()/2)
        self.init_ui()

    def mousePressEvent(self, e):
        QLabel.mousePressEvent(self, e)
        self.event_pos = e.pos()

    def mouseMoveEvent(self, e):
        if e.buttons() != Qt.LeftButton:
            return

        mimeData = QMimeData()
        drag = QDrag(self)
        drag.setMimeData(mimeData)
        drag.exec_(Qt.MoveAction)

    def init_ui(self):
        if self._number == 0:
            pixmap = QPixmap('resources/monitor1.png')
        elif self._number == 1:
            pixmap = QPixmap('resources/monitor2.png')
        else:
            pixmap = QPixmap('resources/screen.png')

        pixmap = pixmap.scaled(self.geometry.width(), self.geometry.height(), PyQt5.QtCore.Qt.KeepAspectRatio)
        self.setPixmap(pixmap)
        self.show()



