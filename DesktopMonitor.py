import sys
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtCore import Qt
from Desktop import ScreenWidget


class DesktopMonitor(QWidget):
    def __init__(self):
        super().__init__()
        self._desktop = QApplication.desktop()
        QApplication.processEvents()
        self.screens = []
        self.moveable_srceen = None
        self.init_ui()

    def init_ui(self):
        self.setAcceptDrops(True)
        self.setGeometry(200, 200, 800, 600)
        for i in range(0, self._desktop.screenCount()):
            self.screens.append(ScreenWidget(desktop = self._desktop,
                                             frame= self.geometry(),
                                             number = i,
                                             parent = self))

    def dragEnterEvent(self, e):
        self.moveable_srceen = e.source()
        e.accept()

    def dropEvent(self, e):
        position = e.pos()
        if self.moveable_srceen.event_pos:
            self.moveable_srceen.move(position - self.moveable_srceen.event_pos)
        else:
            self.moveable_srceen.move(position)
        e.setDropAction(Qt.MoveAction)
        e.accept()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = DesktopMonitor()
    ex.show()
    app.exec_()